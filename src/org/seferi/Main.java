package org.seferi;


import org.seferi.AI.LevelEasy;
import org.seferi.AI.LevelHard;
import org.seferi.AI.LevelMedium;
import org.seferi.AI.MiniMax;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.seferi.Service.*;

public class Main {

    public static boolean aiFirst = false;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        menu();


    }

    //Gets the options to start the game
    public static void menu() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Please enter options to start the game or press exit to quit. \n" +
                    "You can choose an AI level from easy, medium and hard. Enter the options like this: \n" +
                    "start user easy");
            System.out.println("Input command:");
            String input = reader.readLine();
            gameState = GameState.GAME_NOT_FINISHED;
            if (input.equals("exit")) {
                return;
            }
            String[] inputArray = input.split(" ");

            if (checkParameters(input)) {
                cleanField();
                displayGameField();

                if (inputArray[1].equals("easy") && inputArray[2].equals("easy")) {
                    LevelEasy.aiVSai();

                } else if (inputArray[1].equals("user") && inputArray[2].equals("easy")) {
                    aiFirst = false;
                    LevelEasy.userVSai(false);

                } else if (inputArray[1].equals("easy") && inputArray[2].equals("user")) {
                    aiFirst = true;
                    LevelEasy.userVSai(true);
                } else if (inputArray[1].equals("medium") && inputArray[2].equals("medium")) {
                    LevelMedium.aiVSai();
                } else if (inputArray[1].equals("user") && inputArray[2].equals("medium")) {
                    aiFirst = false;
                    LevelMedium.userVSai(false);
                } else if (inputArray[1].equals("medium") && inputArray[2].equals("user")) {
                    aiFirst = true;
                    LevelMedium.userVSai(true);
                } else if (inputArray[1].equals("hard") && inputArray[2].equals("hard")) {
                    LevelHard.aiVSai();
                } else if (inputArray[1].equals("user") && inputArray[2].equals("hard")) {
                    aiFirst = false;
                    LevelHard.userVSai(false);
                } else if (inputArray[1].equals("hard") && inputArray[2].equals("user")) {
                    aiFirst = true;
                    LevelHard.userVSai(true);
                } else if (inputArray[1].equals("user") && inputArray[2].equals("test")) {
                    aiFirst = false;
                    MiniMax miniMax = new MiniMax();
                    miniMax.userVSai(aiFirst);
                } else {
                    userVSuser();
                }
                System.out.println(gameState.getMessage());
            }

        }
    }

    //Fills the array (game field) with empty spaces
    public static void cleanField() {
        for (int i = 0; i < 3; i++) {
            Arrays.fill(field[i], ' ');
        }
    }

    public static void assignPlayers(String input) {
        String[] inputArray = input.split(" ");
        //TODO: WHAT TO DO WITH THE GAME START, HOW TO MAKE IT SIMPLER..
    }

    //Checks if the given input (game options) are within the specifics
    public static boolean checkParameters(String parameters) {
        ArrayList<String> options = new ArrayList<String>(List.of("start", "user", "easy", "medium", "hard"));
        String[] inputArray = parameters.split(" ");
        if (inputArray.length < 3 || !options.contains(inputArray[0]) || !options.contains(inputArray[1]) || !options.contains(inputArray[2])) {
            System.out.println("Bad parameters!");
            return false;
        } else {
            return true;
        }
    }

    //user vs user
    public static void userVSuser() throws IOException {
        while (gameState == GameState.GAME_NOT_FINISHED) {
            makeUserMove();
            playerTurn = playerTurn == 'X' ? 'O' : 'X';
        }
    }

    //get and make moves for players
    public static void makeUserMove() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.println("Enter the coordinates:");
            String coordinates = reader.readLine();
            if (checkCoordinates(coordinates)) {
                String[] coordinatesSplit = coordinates.split(" ");
                int row = Integer.parseInt(coordinatesSplit[0]);
                int column = Integer.parseInt(coordinatesSplit[1]);
                field[row - 1][column - 1] = playerTurn;
                displayGameField();
                gameState = analyzeCells();
                break;
            }
        }
    }
}

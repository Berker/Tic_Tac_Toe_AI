package org.seferi.AI;

import org.seferi.GameState;

import java.io.IOException;
import java.util.ArrayList;

import static org.seferi.Main.makeUserMove;
import static org.seferi.Service.*;

public interface AI {

    static class Move {
        int row, col;
    }

    static void getMove() {
    }

    static void aiVSai() {}

    static void userVSai(boolean aiFirst) {
    }

    static ArrayList<Move> getFreeCoordinates() {
        ArrayList<Move> freeCoordinates = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (field[i][j] == ' ') {
                    Move move = new Move();
                    move.row = i;
                    move.col = j;
                    freeCoordinates.add(move);
                }
            }
        }
        return freeCoordinates;
    }
}

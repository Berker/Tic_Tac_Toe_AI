package org.seferi.AI;

import org.seferi.GameState;

import java.io.IOException;
import java.util.ArrayList;

import static org.seferi.Main.makeUserMove;
import static org.seferi.Service.*;

public class MiniMax {
    static class Move {
        int index;
        int score;
    }

    ArrayList<Character> board = getBoard();
    // AI player
    char aiPlayer = 'X';
    // Human player
    char huPlayer = 'O';


    public void getMove(char computerPlayer, char humanPlayer) {
        aiPlayer = computerPlayer;
        huPlayer = humanPlayer;

        Move move = minimax(board, aiPlayer);
        int count = move.index;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                count--;
                if (count == 0) {
                    System.out.println("Making move level \"hard\"");
                    field[i][j] = playerTurn;
                    displayGameField();
                    gameState = analyzeCells();
                }
            }
        }

    }


    public void userVSai(boolean aiFirst) throws IOException {
        while (gameState == GameState.GAME_NOT_FINISHED) {
            if (!aiFirst) {
                gameState = analyzeCells();
                makeUserMove();
                playerTurn = playerTurn == 'X' ? 'O' : 'X';
                gameState = analyzeCells();
                getMove('O', 'X');
            } else {
                gameState = analyzeCells();
                getMove('X', 'O');
                playerTurn = playerTurn == 'X' ? 'O' : 'X';
                gameState = analyzeCells();
                makeUserMove();
            }
            playerTurn = playerTurn == 'X' ? 'O' : 'X';
        }
    }


    ArrayList<Character> getBoard() {
        ArrayList<Character> board = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board.add(field[i][j]);
            }
        }
        return board;
    }

    // Returns indexes of empty spots on the board
    ArrayList<Integer> getEmptySpots(ArrayList<Character> board) {
        ArrayList<Integer> emptySpots = new ArrayList<>();
        for (int i = 0; i < board.size(); i++) {
            if (board.get(i) == ' ') {
                emptySpots.add(i);
            }
        }
        return emptySpots;
    }

    //Winning combinations using the board indexes
    boolean winning(ArrayList<Character> board, char player) {
        if ((board.get(0) == player && board.get(1) == player && board.get(2) == player) ||
                (board.get(3) == player && board.get(4) == player && board.get(5) == player) ||
                (board.get(6) == player && board.get(7) == player && board.get(8) == player) ||
                (board.get(0) == player && board.get(3) == player && board.get(6) == player) ||
                (board.get(1) == player && board.get(4) == player && board.get(7) == player) ||
                (board.get(2) == player && board.get(5) == player && board.get(8) == player) ||
                (board.get(0) == player && board.get(4) == player && board.get(8) == player) ||
                (board.get(2) == player && board.get(4) == player && board.get(6) == player)
        ) {
            return true;
        } else {
            return false;
        }
    }


    //Minimax algorithm
    Move minimax(ArrayList<Character> newBoard, char player) {
        int score = 0;
        //Available spots to play on the board
        ArrayList<Integer> availSpots = getEmptySpots(newBoard);
        // checks for the terminal states such as win, lose, and tie and returning a value accordingly
        if (winning(newBoard, huPlayer)) {
            score = -10;
        } else if (winning(newBoard, aiPlayer)) {
            score = 10;
        } else if (availSpots.size() == 0) {
            score = 0;
        }

        // An array to collect all the moves
        ArrayList<Move> moves = new ArrayList<>();

        for (int i = 0; i < availSpots.size(); i++) {
            Move move = new Move();
            // Store index of the available spot
            move.index = newBoard.get(availSpots.get(i));
            // Set the empty spot to the current player
            newBoard.set(move.index, player);

            /* collect the score resulted from calling minimax
              on the opponent of the current player */

            if (player == aiPlayer) {
                Move result = minimax(newBoard, huPlayer);
                move.score = score;
            } else {
                Move result = minimax(newBoard, aiPlayer);
                move.score = score;
            }

            // Reset the spot to empty.
            newBoard.set(move.index, ' ');

            // Add move to the arrayList
            moves.add(move); //TODO: TRY TO EXTRACT THIS METHOD, ADDING MOVES TO MOVES ARRAY

        }

        int bestMove = 0;
        // if it is the computer's turn loop over the moves and choose the move with the highest score
        if (player == aiPlayer) {
            int bestScore = -10000;
            for (int i = 0; i < moves.size(); i++) {
                if (moves.get(i).score > bestScore) {
                    bestScore = moves.get(i).score;
                    bestMove = i;
                }
            }
        } else {
            // else loop over the moves and choose the move with the lowest score
            int bestScore = 10000;
            for (int i = 0; i < moves.size(); i++) {
                if (moves.get(i).score < bestScore) {
                    bestScore = moves.get(i).score;
                    bestMove = i;
                }
            }
        }

// return the chosen move (object) from the moves array
        return moves.get(bestMove);

    }

}

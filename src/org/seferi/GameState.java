package org.seferi;

public enum GameState {
    GAME_NOT_FINISHED("Game not finished"),
    DRAW("Draw"),
    IMPOSSIBLE("Impossible"),
    X_WINS("X wins"),
    O_WINS("O wins");

    String message;

    GameState(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
